#pragma once
#include <SDL2/SDL.h>

struct window_properties {
    uint32_t width = 250;
    uint32_t height = 250;
    SDL_Window* window_handle = nullptr;
    SDL_Renderer* renderer_handle = nullptr;
};

extern window_properties g_window_properties;

bool init_window(window_properties& properties);

void set_draw_color(window_properties& properties, uint8_t r, uint8_t g, uint8_t b);
void draw_pixel(window_properties& properties, uint32_t x, uint32_t y);
void draw_circle(window_properties& properties, uint32_t x, uint32_t y, uint32_t radius);
void clear(window_properties& properties);

uint32_t get_width(window_properties& properties);
uint32_t get_height(window_properties& properties);

extern "C" {
    void global_set_draw_color(uint8_t r, uint8_t g, uint8_t b);
    void global_draw_pixel(uint32_t x, uint32_t y);
    void global_draw_circle(uint32_t x, uint32_t y, uint32_t radius);
    void global_clear();

    uint32_t global_get_width();
    uint32_t global_get_height();
}