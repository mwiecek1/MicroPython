extern "C"{
#include "port/micropython_embed.h"
#include "py/compile.h"
}
#include <string.h>
#include <SDL2/SDL.h>
#include "sdl_helper.hpp"
#include <string>
#include <fstream>
#include <streambuf>


#define LOG(txt, ...) printf("%s[%d]: " txt "\n", __func__, __LINE__, ##__VA_ARGS__)

// This array is the MicroPython GC heap.
static char heap[128 * 1024];

int temperature = 0;

extern "C"{
int32_t get_temperature(){
    return temperature;
}
}

mp_obj_t compile_python(const std::string& module_name, const std::string& code) {
    //Parse python code
    mp_lexer_t *lex = mp_lexer_new_from_str_len(MP_QSTR__lt_stdin_gt_, code.c_str(), code.size(), 0);

    //Generate micropython string
    qstr source_name = qstr_from_strn(module_name.c_str(), module_name.size());

    //Parse it
    mp_parse_tree_t parse_tree = mp_parse(lex, MP_PARSE_FILE_INPUT);

    //Compile
    mp_obj_t module_fun = mp_compile(&parse_tree, source_name, true);

    return module_fun;
}

void run(mp_obj_t& module){
    nlr_buf_t nlr;
    //Push stack
    if (nlr_push(&nlr) == 0) {
        //Call compiled code
        mp_call_function_0(module);

        //Pop stack
        nlr_pop();
    } else {
        // Uncaught exception: print it out.
        mp_obj_print_exception(&mp_plat_print, (mp_obj_t)nlr.ret_val);
    }
}

std::string read_file(const std::string& file_path){
    std::ifstream t(file_path);
    std::string str((std::istreambuf_iterator<char>(t)),
                 std::istreambuf_iterator<char>());
    return str;
}

std::string scripts[] = {
    "examples/api.py",
    "examples/time_1_digital.py",
    "examples/temperature_display.py",
    "examples/no_access_to_files.py"
};

int main() {
    std::string script_to_load = scripts[1];

    bool reaload_script = false;
    int quit = 0;
    int stack_top;

    mp_embed_init(&heap[0], sizeof(heap), &stack_top);

    bool init_staus = init_window(g_window_properties);
    if(!init_staus){
        printf("Failed to init window\n");
        return 1;
    }
    LOG("Window init done");

    LOG("Read file [%s]", script_to_load.c_str());
    auto python_script = read_file(script_to_load);
    LOG("Read file -> done");

    LOG("Python compilation");
    mp_obj_t compiled_module = compile_python(script_to_load, python_script);
    LOG("Python compilation -> done");

    while(!quit)
    {
        SDL_Event e;

        while(SDL_PollEvent(&e)){

            switch(e.type){
                case SDL_QUIT:
                    quit = 1;
                    break;
                case SDL_KEYUP:
                    switch(e.key.keysym.sym){
                        case SDLK_r:
                            reaload_script = true;
                            break;
                        case SDLK_UP:
                            temperature += 5;
                            LOG("Update temperature to: %d", temperature);
                            break;
                        case SDLK_DOWN:
                            temperature -= 5;
                            LOG("Update temperature to: %d", temperature);
                            break;
                        case SDLK_0:
                            script_to_load = scripts[0];
                            reaload_script = true;
                            break;
                        case SDLK_1:
                            script_to_load = scripts[1];
                            reaload_script = true;
                            break;
                        case SDLK_2:
                            script_to_load = scripts[2];
                            reaload_script = true;
                            break;
                        case SDLK_3:
                            script_to_load = scripts[3];
                            reaload_script = true;
                            break;
                    }
            }
        }

        if(reaload_script) {
            LOG("Reloading script %s", script_to_load.c_str());

            reaload_script = false;
            python_script = read_file(script_to_load);
            compiled_module = compile_python(script_to_load, python_script);

            LOG("Reloading script -> done");
        }

        run(compiled_module);
        SDL_RenderPresent(g_window_properties.renderer_handle);
    }

    // Deinitialise MicroPython.
    mp_embed_deinit();
    SDL_Quit();


    return 0;
}
