# This file is based on Makefile from micropython/examples/embedding

EMBED_DIR = micropython_embed
APP_DIR = ..
PROG = widget_app


CFLAGS += -I.
CFLAGS += -I$(EMBED_DIR)
CFLAGS += -I$(EMBED_DIR)/port
CFLAGS += -Wall -O3 -fno-common

SRC += $(APP_DIR)/main.cpp
SRC += $(wildcard $(EMBED_DIR)/*/*.c) $(wildcard $(EMBED_DIR)/*/*/*.c)
SRC += $(APP_DIR)/modules/widget/widget.c
OBJ += $(SRC:.c=.o)

$(PROG): $(OBJ)
	g++ -o $@ $^ $(shell sdl2-config --cflags --libs) $(CFLAGS) $(APP_DIR)/sdl_helper.cpp

clean:
	/bin/rm -f $(OBJ) $(PROG)
