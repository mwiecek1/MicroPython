# Micropython example application

Simple application to play with micropython. Application allows to create graphical widgets using micropython with custom module embedded into c++ application. To interact with graphics application uses SDL2 library.
In examples directory You can find few python scripts to display various data to screen. You can which one is running with key from 0 to 3.  You can increase temperature "sensor" with arrow up and down keys.

## How to

    #Clone repository and submodules
        git clone --recursive https://gitlab.com/mwiecek1/MicroPython.git
    #Enter freshly clonned repository
        cd MicroPython
    #Build application
        sh build_application.sh
    #Run binary
        ./build/widget_app

## Python api
Currently widget module provides methods:
* get_time
* get_temperature
* get_width
* get_height
* draw_point
* draw_circle
* clear
* set_draw_color

