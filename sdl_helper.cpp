#include "sdl_helper.hpp"

#include <cmath>

window_properties g_window_properties;

bool init_window(window_properties& properties){
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
        return false;
    }

    properties.window_handle = SDL_CreateWindow("Widgets",
                                       SDL_WINDOWPOS_CENTERED,
                                       SDL_WINDOWPOS_CENTERED,
                                       properties.width, properties.height, 0);

    if(!properties.window_handle ){
        printf("error SDL_CreateWindow: %s\n", SDL_GetError());
        return false;
    }

    properties.renderer_handle = SDL_CreateRenderer(properties.window_handle, -1, SDL_RENDERER_ACCELERATED);

    if(!properties.renderer_handle){
        printf("error SDL_CreateRenderer: %s\n", SDL_GetError());
        return false;
    }

    return true;
}

void set_draw_color(window_properties& properties, uint8_t r, uint8_t g, uint8_t b){
    if(SDL_SetRenderDrawColor(properties.renderer_handle, r, g, b, 0) != 0){
        printf("error SDL_SetRenderDrawColor: %s\n", SDL_GetError());
    }
}

void clear(window_properties& properties){
    if(SDL_RenderClear(properties.renderer_handle) != 0){
        printf("error SDL_RenderClear: %s\n", SDL_GetError());
    }
}

void draw_pixel(window_properties& properties, uint32_t x, uint32_t y){
    if(SDL_RenderDrawPoint(properties.renderer_handle, x, y) != 0) {
        printf("error SDL_RenderDrawPoint: %s\n", SDL_GetError());
    }
}

void draw_circle(window_properties& properties, uint32_t x, uint32_t y, uint32_t radius) {
    int32_t x_end = x + radius;
    int32_t y_end = y + radius;

    for(int32_t y_pos = y - radius; y_pos < y_end; ++y_pos) {
        for(int32_t x_pos = x - radius; x_pos < x_end; ++x_pos) {
            auto distance = std::sqrt((x_pos - x)*(x_pos - x) + (y_pos - y)*(y_pos - y));
            if(distance < radius){
                draw_pixel(properties, x_pos, y_pos);
            }
        }
    }
}

uint32_t get_width(window_properties& properties) {
    return properties.width;
}

uint32_t get_height(window_properties& properties) {
    return properties.height;
}

extern "C" {
    void global_set_draw_color(uint8_t r, uint8_t g, uint8_t b) {
        set_draw_color(g_window_properties, r, g, b);
    }

    void global_draw_pixel(uint32_t x, uint32_t y){
        draw_pixel(g_window_properties, x, y);
    }

    void global_draw_circle(uint32_t x, uint32_t y, uint32_t radius){
        draw_circle(g_window_properties, x, y, radius);
    }

    void global_clear(){
        clear(g_window_properties);
    }

    uint32_t global_get_width(){
        return get_width(g_window_properties);
    }

    uint32_t global_get_height(){
        return get_height(g_window_properties);
    }
}