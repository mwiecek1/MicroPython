import widget

CIRCLE_SIZE = 5
DIGIT_WIDTH = 5*6
DIGIT_HEIGHT = 5*5

mapping = [
    #0
    [0b001100,
     0b010010,
     0b010010,
     0b010010,
     0b001100],
    #1
    [0b000010,
     0b000010,
     0b000010,
     0b000010,
     0b000010],
    #2
    [0b001100,
     0b010010,
     0b000100,
     0b001000,
     0b011110],
    #3
    [0b001100,
     0b010010,
     0b000100,
     0b010010,
     0b001100],
    #4
    [0b000110,
     0b001010,
     0b011110,
     0b000010,
     0b000010],
    #5
    [0b011110,
     0b010000,
     0b011100,
     0b000010,
     0b011100],
    #6
    [0b001100,
     0b010000,
     0b011100,
     0b010010,
     0b001100],
    #7
    [0b001110,
     0b010010,
     0b000100,
     0b001000,
     0b010000],
     #8
    [0b011110,
     0b010010,
     0b011110,
     0b010010,
     0b011110],
     #9
    [0b001100,
     0b010010,
     0b001110,
     0b000010,
     0b001100],
]

def draw_circle(pos_x, pos_y):
    pts = [[0,0,1,0,0],
           [0,1,1,1,0],
           [1,1,1,1,1],
           [0,1,1,1,0],
           [0,0,1,0,0]]

    for y in range(5):
        for x in range(5):
            if pts[int(y)][int(x)] == 1:
                widget.draw_pixel(int(pos_x + x), int(pos_y + y))

def draw_line(x, y, line):
    x += 5*6
    for _ in range(6):
        if line % 2 == 1:
            draw_circle(x,y)
        x -= 5
        line = line >> 1

def get_mapping(digit):
    return mapping[digit]

def draw_digit(x,y, digit):
    mapped = get_mapping(digit)
    for line in mapped:
        draw_line(x, y, line)
        y += 5

def draw_2_digits(x,y, number):
    first_digit = number // 10
    second_digit = number % 10

    draw_digit(x, y, first_digit)
    draw_digit(x + DIGIT_WIDTH,y, second_digit)

widget.set_draw_color(255, 255, 255)
widget.clear()
widget.set_draw_color(0, 0, 0)

time = widget.get_time()

hours = time[3]
minutes = time[4]
seconds = time[5]

pos_height = (widget.get_height() + DIGIT_HEIGHT)//2 - DIGIT_HEIGHT
draw_2_digits(10, pos_height, hours)
draw_2_digits(10 + 2 * DIGIT_WIDTH, pos_height, minutes)
draw_2_digits(10 + 4 * DIGIT_WIDTH, pos_height, seconds)
