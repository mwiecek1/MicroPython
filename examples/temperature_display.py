import widget

def draw_rectangle(x, y, width, height):
    for _y in range(height):
        for _x in range(width):
            widget.draw_pixel(x+_x, y+_y)

def draw_hot_day():
    widget.set_draw_color(100, 0, 250)
    widget.clear()

    widget.set_draw_color(255,50,0)
    widget.draw_circle(widget.get_width()//2, widget.get_height()//2, 100)

def draw_sunny_day():
    widget.set_draw_color(60, 20, 255)
    widget.clear()

    widget.set_draw_color(255,255,0)
    widget.draw_circle(widget.get_width()//2, widget.get_height()//2, 100)

def draw_normal_day():
    widget.set_draw_color(0, 20, 255)
    widget.clear()

    widget.set_draw_color(225,255,0)
    widget.draw_circle(widget.get_width()//2, widget.get_height()//2, 50)

def draw_cold_day():
    widget.set_draw_color(0, 20, 30)
    widget.clear()


    snowflake = [
        0b0000001000000,
        0b0000101010000,
        0b1010011100101,
        0b0100001000010,
        0b1010001000101,
        0b0001001001000,
        0b1000101010001,
        0b0100010100010,
        0b1111111111111,
        0b0100010100010,
        0b1000101010001,
        0b0001001001000,
        0b1010001000101,
        0b0100001000010,
        0b1010011100101,
        0b0000101010000,
        0b0000001000000,
    ]

    widget.set_draw_color(0,0,220)
    y_offset = 0
    for line in snowflake:
        x_offset = 0
        for _ in range(14):
            if line &0b1 == 1:
                widget.draw_circle(200 - x_offset, 20 + y_offset, 6)
            x_offset += 12
            line >>= 1
        y_offset += 12

def draw_termometer(temp):
    temp_to_show = temp
    if temp_to_show < -60:
        temp_to_show = -60
    if temp_to_show > 150:
        temp_to_show = 150

    temp_to_show = temp_to_show + 75

    widget.set_draw_color(255, 255, 255)
    draw_rectangle(10, 10, 5, widget.get_height() - 20)
    widget.draw_circle(12, widget.get_height() - 10, 7)

    widget.set_draw_color(75, 75, 75)
    draw_rectangle(10, widget.get_height() - 95, 5, 1)

    widget.set_draw_color(0, 0, 0)
    draw_rectangle(11, widget.get_height() - 20 - temp_to_show, 3, temp_to_show+11)
    widget.draw_circle(12, widget.get_height() - 10, 5)

temp = widget.get_temperature()

if temp > 26:
    draw_hot_day()
elif temp > 15:
    draw_sunny_day()
elif temp >= 0:
    draw_normal_day()
else:
    draw_cold_day()

draw_termometer(temp)