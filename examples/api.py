import widget

widget.set_draw_color(0,0,0)
widget.clear()

widget.set_draw_color(125, 125, 125)
widget.draw_circle(125, 125, 75)

widget.set_draw_color(255,0,0)
for i in range(widget.get_width()):
    widget.draw_pixel(i, i)


time = widget.get_time()
seconds = time[5]

widget.set_draw_color(0, seconds*4, 0)
widget.draw_circle(200, 200, 10)

widget.set_draw_color(0, 0, widget.get_temperature())
widget.draw_circle(45, 45, 10)