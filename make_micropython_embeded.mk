# This file is based on micropython_embed.mk from micropython project

# Set the location of the top of the MicroPython repository.
MICROPYTHON_TOP = ../micropython

USER_C_MODULES = ../modules/widget/micropython.mk all

# Include the main makefile fragment to build the MicroPython component.
include $(MICROPYTHON_TOP)/ports/embed/embed.mk