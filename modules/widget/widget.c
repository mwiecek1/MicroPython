#include "py/obj.h"
#include "py/runtime.h"
#include "py/builtin.h"

#include <time.h>

void global_draw_pixel(uint32_t x, uint32_t y);
void global_draw_circle(uint32_t x, uint32_t y, uint32_t radius);
void global_set_draw_color(uint8_t r, uint8_t g, uint8_t b);
void global_clear();
uint32_t global_get_width();
uint32_t global_get_height();

int32_t get_temperature();

static mp_obj_t widget_get_time(){
    mp_obj_t time_tuple[6]; //year, month, day, hour, minutes, seconds

    time_t t = time(0);   // get time now
    struct tm* now = localtime(&t);

    time_tuple[0] = mp_obj_new_int(now->tm_year + 1900);
    time_tuple[1] = mp_obj_new_int(now->tm_mon);
    time_tuple[2] = mp_obj_new_int(now->tm_mday);
    time_tuple[3] = mp_obj_new_int(now->tm_hour);
    time_tuple[4] = mp_obj_new_int(now->tm_min);
    time_tuple[5] = mp_obj_new_int(now->tm_sec);

    return mp_obj_new_tuple(6, time_tuple);
}

static MP_DEFINE_CONST_FUN_OBJ_0(widget_get_time_obj, widget_get_time);

static mp_obj_t widget_get_temperature(){
    return mp_obj_new_int(get_temperature());
}

static MP_DEFINE_CONST_FUN_OBJ_0(widget_get_temperature_obj, widget_get_temperature);

static mp_obj_t widget_draw_pixel(mp_obj_t x_obj, mp_obj_t y_obj) {
    int x = mp_obj_get_int(x_obj);
    int y = mp_obj_get_int(y_obj);

    global_draw_pixel(x,y);

    return mp_const_none;
}

static MP_DEFINE_CONST_FUN_OBJ_2(widget_draw_pixel_obj, widget_draw_pixel);

static mp_obj_t widget_set_draw_color(size_t n_args, const mp_obj_t *args) {
    // Extract the ints from the micropython input objects
    int r = mp_obj_get_int(args[0]);
    int g = mp_obj_get_int(args[1]);
    int b = mp_obj_get_int(args[2]);

    global_set_draw_color(r, g, b);

    return mp_const_none;
}

static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(widget_set_draw_color_obj, 3, 3, widget_set_draw_color);

static mp_obj_t widget_draw_circle(size_t n_args, const mp_obj_t *args) {
    // Extract the ints from the micropython input objects
    int x = mp_obj_get_int(args[0]);
    int y = mp_obj_get_int(args[1]);
    int radius = mp_obj_get_int(args[2]);

    global_draw_circle(x, y, radius);

    return mp_const_none;
}

static MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(widget_draw_circle_obj, 3, 3, widget_draw_circle);

static mp_obj_t widget_clear() {
    global_clear();

    return mp_const_none;
}

static MP_DEFINE_CONST_FUN_OBJ_0(widget_clear_obj, widget_clear);

static mp_obj_t widget_get_width() {
    return mp_obj_new_int(global_get_width());
}

static MP_DEFINE_CONST_FUN_OBJ_0(widget_get_width_obj, widget_get_width);

static mp_obj_t widget_get_height() {
    return mp_obj_new_int(global_get_height());
}

static MP_DEFINE_CONST_FUN_OBJ_0(widget_get_height_obj, widget_get_height);

static const mp_rom_map_elem_t widget_module_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR_get_time), MP_ROM_PTR(&widget_get_time_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_temperature), MP_ROM_PTR(&widget_get_temperature_obj) },

    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_widget) },
    { MP_ROM_QSTR(MP_QSTR_draw_pixel), MP_ROM_PTR(&widget_draw_pixel_obj) },
    { MP_ROM_QSTR(MP_QSTR_draw_circle), MP_ROM_PTR(&widget_draw_circle_obj) },
    { MP_ROM_QSTR(MP_QSTR_set_draw_color), MP_ROM_PTR(&widget_set_draw_color_obj) },
    { MP_ROM_QSTR(MP_QSTR_clear), MP_ROM_PTR(&widget_clear_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_width), MP_ROM_PTR(&widget_get_width_obj) },
    { MP_ROM_QSTR(MP_QSTR_get_height), MP_ROM_PTR(&widget_get_height_obj) },
};
static MP_DEFINE_CONST_DICT(widget_module_globals, widget_module_globals_table);

const mp_obj_module_t widget_user_cmodule = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&widget_module_globals,
};

MP_REGISTER_MODULE(MP_QSTR_widget, widget_user_cmodule);
