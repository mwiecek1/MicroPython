#remove build directory
rm -rf build

#make build directory
mkdir build

#copy mpconfigport.h to build directory
cp -v mpconfigport.h build

#go to build directory
cd build

#First make micropython
make -f ../make_micropython_embeded.mk

#Build application
make -f ../make_application.mk

echo "Done! Run application ./build/widget_app"